package eu.mclive.BungeeChatLog;

import java.util.Date;
import java.util.logging.Logger;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;

import eu.mclive.BungeeChatLog.MySQL.*;

public class BungeeChatLog extends Plugin {
	
	public static BungeeChatLog INSTANCE;
	public final Logger logger = getLogger();
	public static MySQL sql;
	public MySQLHandler sqlHandler;
	public ChatLogConfiguration config;
	Long pluginstart = null;
	public static boolean debug;
	
	public void onEnable() {
		BungeeChatLog.INSTANCE = this;
		
		//Needed to create the config. Not the whole debug part but yea.
		//Just the getConfiguration() call.
		debug = getConfiguration().getConfig().getBoolean("debug");
		
		getProxy().getPluginManager().registerCommand(this, new ChatReport(this));
		this.getProxy().getPluginManager().registerListener(this, new Events(this));
		
        Date now = new Date();
        pluginstart = new Long(now.getTime()/1000);
		
	}

	public ChatLogConfiguration getConfiguration(){
		if(this.config == null) config = new ChatLogConfiguration(this);
		return this.config;
	}
	
	public Configuration getConfig(){ return getConfiguration().getConfig(); }
	
	public void saveConfig(){ getConfiguration().saveConfig(); }
	
}
