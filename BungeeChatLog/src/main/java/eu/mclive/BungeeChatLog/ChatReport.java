package eu.mclive.BungeeChatLog;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ChatReport extends Command {

    private BungeeChatLog plugin;

	public ChatReport(BungeeChatLog plugin) {
        super("testcommand");
        this.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
          //p.sendMessage( new ComponentBuilder("Created your project!").color(ChatColor.GOLD).create());
          sender.sendMessage(ChatColor.RED + "Only Players can run this command!");
        } else {

		final ProxiedPlayer p = (ProxiedPlayer) sender;
		
		if(p.getServer().getInfo().getName().equals("lobby")) {
			p.chat("/testcommand");
			return;
		} else {
			p.sendMessage("not on lobby!");
		}
		
			if (args.length == 0 || args.length > 1) {
				p.sendMessage("�7�m                                                                     ");
				p.sendMessage("�e/chatreport2 <playername> �7- �agets the Chatlog from a player.");
				p.sendMessage("�7�m                                                                     ");
			} else if (args.length == 1) {
				final String p2 = args[0];
				plugin.getProxy().getScheduler().runAsync(plugin, new Runnable() {
					public void run() {
			            Date now = new Date();
			            Long timestamp = new Long(now.getTime()/1000);
			            boolean mode = plugin.getConfig().getBoolean("minigamesmode", false);
			            String server = p.getServer().getInfo().getName();
			            Long pluginstart = plugin.pluginstart;
			            if(mode == false) { //disabled minigame mode? Only get messages from last 15 minutes!
			            	Calendar cal = Calendar.getInstance();
			            	cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)-15); //15 minutes before
			            	plugin.pluginstart = cal.getTimeInMillis() / 1000;
			            }
			            int messagesSent = plugin.sqlHandler.checkMessage(server, p2, pluginstart, timestamp);
			            if(messagesSent >= 1) {
			            	plugin.logger.info("[" + p.getName() + "] getting ChatLog from " + p2);
			            	String reportid = UUID.randomUUID().toString().replace("-", "");
			            	plugin.sqlHandler.setReport(p.getServer().getInfo().getName(), p2, plugin.pluginstart, timestamp, reportid);
			            	//String URL = getConfig().getString("URL");
			            	String URL = "Test";
			            	p.sendMessage("�eURL: �a" + URL + reportid);
			            } else {
			            	p.sendMessage("�cNo messages found from " + p2);
			            }
			    	}
			    });
			}
			
		}

    }
}
